#!/usr/bin/env node

"use strict";

const fs = require("fs");
const program = require("commander");
const rivTranslator = require("./rivertranslator");

console.log("River package translator");

program
    .arguments("<intputFile> <outputFile>")
    .action(function (inputFile, outputFile) {
        try {
            console.log("Input file: %s\nOutput file: %s\n", inputFile, outputFile);

            var payloadString = fs.readFileSync(inputFile, "utf8");
            var inputData = JSON.parse(payloadString);
            rivTranslator.translate(inputData, outputFile);
        } catch (err) {
            console.error(err.stack);
            process.exit(1);
        }
    })
    .parse(process.argv);
