FROM node:alpine

COPY . /opt/app
WORKDIR /opt/app

VOLUME /input
VOLUME /output