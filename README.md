# River Translator#

River translator is a command line Node.js application that translates cell level river boundary conditions to RIV pakcage file. It is built on Node.js V6.9.3.

### Command Line Format ###

1. node index <file path of payload.json> <file path of *.riv>
1. translateriver <file path of payload.json> <file path of *.riv>

### Content of payload.json file ###

```
#!javascript

{
	"modelRunTimeSchedule": {
		"startDateTime": "2000-01-01",
        "unit": "day",
        "relative": true,
        "schedule": [[ 0, 5], [5, 10], [10, 15], [15, 20], [20, 25] ]
	},
	"riverGroups": [
		{
            "group": "riv1",
            "groupCells": [1,2,4,5,7,8],
            "timeSchedule": {
                "startDateTime": "2000-01-01",
                "unit": "day",
                "relative": true,
                "schedule": [ [0, 10], [10, 20] ]
            },
            "attributes": [
                {"timeSpan": 0, "rivStage": [200, 200, 200, 200, 200, 200], "conductance": [1, 1, 1, 1, 1, 1], "rivBottom": [100, 100, 100, 100, 100, 100]},
                {"timeSpan": 1, "rivStage": [250, 250, 250, 250, 250, 250], "conductance": [1, 1, 1, 1, 1, 1], "rivBottom": [100, 100, 100, 100, 100, 100]}
            ]
        }
	],
    "rivPackageSettings": {
        "IRIVCB": 1,
        "lengthUnitFactor": 1,
        "conductanceUnitFactor": 2
    }
}

```

###### where conductance = river bed conductance per unit length * the reach length of river in each cell, its unit is L^2/T ######

### task manager call
river input as the dataPath of the payload json file:
```
#!javascript

{
    "taskid":"taskid",
        "input": [
        {
            "name": "riverinput",
            "format": "json",
            "dataPath": "/storage/riverinput.json"
        }
    ],
    "outputConfig": [
        {
            "name": "riveroutput",
            "format": "riv",
            "dataPath": "/storage/riverresult.riv"
        }
    ]
}

```