"use strict";

const fs = require("fs");

// get the index and time span of the first span arguement in the second schedule argument
var getIndexAndTimeSpan = function (span, schedule) {
    for (let i = 0; i < schedule.length; i++) {
        let t = schedule[i];
        if (span[0] >= t[0] && span[1] <= t[1]) {
            return {
                "index": i,
                "timeSpan": t
            };
        }
    }

    return null;
};

var createRivPackageString = function (riverCells, rivPackageSettings) {
    let rivPackageString = "# RIV: River package file created on " + new Date() + "\n";

    let MXACTR = riverCells.riverCellsCount; // the max number of river reaches
    let IRIVCB = rivPackageSettings.IRIVCB;
    let dataSet2 = "\t" + MXACTR + "\t" + IRIVCB + "\n";
    rivPackageString += dataSet2;

    let riverCellsForAllStressPeriods = riverCells.riverCells;
    for (let i = 0; i < riverCellsForAllStressPeriods.length; i++) {
        let riverCellsForOneStressPeriod = riverCellsForAllStressPeriods[i];
        let ITMP = Object.keys(riverCellsForOneStressPeriod).length; // the number of non-parameter reaches for the current stress period
        let NP = 0; // the number of parameters in use in the current stress period
        let dataSet5 = "\t" + ITMP + "\t" + NP + "\n";
        rivPackageString += dataSet5;

        let dataSet6b = "";
        for (let cell in riverCellsForOneStressPeriod) {
            let stage = riverCellsForOneStressPeriod[cell].rivStage * rivPackageSettings.lengthUnitFactor;
            let cond = riverCellsForOneStressPeriod[cell].cond * rivPackageSettings.conductanceUnitFactor;
            let rbot = riverCellsForOneStressPeriod[cell].rivBottom * rivPackageSettings.lengthUnitFactor;

            dataSet6b += cell + "\t" + stage + "\t" + cond + "\t" + rbot + "\n";
        }

        rivPackageString += dataSet6b;
    }

    return rivPackageString;
};

var translate = function (inputData, rivPackageFullPath) {
    try {
        console.log("Translating RIV package...");

        let modelRunTimeSchedule = inputData["modelRunTimeSchedule"];
        let riverGroups = inputData["riverGroups"];
        let rivPackageSettings = inputData["rivPackageSettings"];

        let riverCellsCount = 0;
        let riverCells = [];

        let modelSchedule = modelRunTimeSchedule.schedule;
        for (let i = 0; i < modelSchedule.length; i++) {
            let modelTimeSpan = modelSchedule[i];
            let riverCellsForOneStressPeriod = {};
            for (let j = 0; j < riverGroups.length; j++) {
                let riverGroup = riverGroups[j];
                let riverGroupSchedule = riverGroup.timeSchedule.schedule;
                let riverGroupCells = riverGroup.groupCells;

                riverCellsCount += riverGroupCells.length;

                let indexSpan = getIndexAndTimeSpan(modelTimeSpan, riverGroupSchedule);
                if (indexSpan === null) {
                    let attributes = riverGroup.attributes[riverGroup.attributes.length - 1];
                    for (let l = 0; l < riverGroupCells.length; l++) {
                        let cell = riverGroupCells[l];
                        let rivStageAtCell = attributes.rivStage[l];
                        let cellCond = attributes.conductance[l];
                        let rivBottomAtCell = attributes.rivBottom[l];
                        riverCellsForOneStressPeriod[cell] = {
                            "rivStage": rivStageAtCell,
                            "cond": cellCond,
                            "rivBottom": rivBottomAtCell
                        };
                    }
                } else {
                    for (let k = 0; k < riverGroup.attributes.length; k++) {
                        if (riverGroup.attributes[k].timeSpan === indexSpan.index) {
                            let rivStage = riverGroup.attributes[k].rivStage;
                            let rivCellsConds = riverGroup.attributes[k].conductance;
                            let rivBottom = riverGroup.attributes[k].rivBottom;
                            for (let l = 0; l < riverGroupCells.length; l++) {
                                let cell = riverGroupCells[l];
                                let rivStageAtCell = rivStage[l];
                                let cellCond = rivCellsConds[l];
                                let rivBottomAtCell = rivBottom[l];

                                riverCellsForOneStressPeriod[cell] = {
                                    "rivStage": rivStageAtCell,
                                    "cond": cellCond,
                                    "rivBottom": rivBottomAtCell
                                };
                            }
                        }
                    }
                }
            }
            riverCells.push(riverCellsForOneStressPeriod);
        }

        riverCellsCount = riverCellsCount / modelSchedule.length;
        let content = createRivPackageString({riverCellsCount, riverCells}, rivPackageSettings);
        fs.writeFileSync(rivPackageFullPath, content, "utf8");

        console.log("River package translation completed!");
    } catch (err) {
        throw (err);
    }
};

module.exports = {
    translate: translate
};
